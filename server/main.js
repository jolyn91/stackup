var express = require("express");
var bodyParser=require('body-parser');

var app = express();

var NODE_PORT = process.env.PORT || 5000;

// app.use("/bower_components", express.static(__dirname + "/../bower_components"));
app.use(express.static(__dirname + "/../client/"));

console.log(__dirname);

// var webroutes = require(__dirname + '/routes/routes.js');
// webroutes.set(app);

app.listen(NODE_PORT, function(){
    console.log("Server running at http://localhost:"+NODE_PORT);
});

app.use(bodyParser.urlencoded({ extended: true }));

