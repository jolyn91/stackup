(function () {
    angular
        .module("StackUp")
        .config(MyAppConfig);

    MyAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function MyAppConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: "app/home/home.html",
            controller: 'HomePageAppCtrl',
            controllerAs: 'ctrl'
        })
        // .state('home', {
        //     url: '/home',
        //     views: {
        //         "navbar@home": {
        //             templateUrl: "app/home/navbar.html"
        //         },
        //         "carousel@home": {
        //             templateUrl: "app/home/carousel.html"
        //         },
        //         "about-us@home": {
        //             templateUrl: "app/home/about-us.html"
        //         },
        //         "coaches-and-instructors@home": {
        //             templateUrl: "app/home/coaches-and-instructors.html"
        //         },
        //         "testimonials@home": {
        //             templateUrl: "app/home/testimonials.html"
        //         },
        //         "events@home": {
        //             templateUrl: "app/home/events-home.html"
        //         },
        //         "footer@home": {
        //             templateUrl: "app/home/footer.html"
        //         }
        //     },
        //     controller: 'HomePageAppCtrl',
        //     controllerAs: 'ctrl'
        // })
        //     .state('home', {
        //         url: '/home',
        //         abstract: true,
        //         template: "",
        //         templateUrl: 'app/home/home.html',
        //         controller: 'HomePageAppCtrl',
        //         controllerAs: 'ctrl'
        //     })
        //     .state('home.navbar', {
        //         templateUrl: 'app/home/navbar.html'
        //     })
        //     .state('home.carousel', {
        //         templateUrl: 'app/home/carousel.html'
        //     })
        //     .state('home.about-us', {
        //         templateUrl: 'app/home/about-us.html'
        //     })
        //     .state('home.coaches-and-instructors', {
        //         templateUrl: 'app/home/coaches-and-instructors.html'
        //     });
            .state('events', {
                url: '/events',
                templateUrl: "app/events/events.html",
                controller: 'HomePageAppCtrl',
                controllerAs: 'ctrl'
            });

        $urlRouterProvider.otherwise("/home");
    }

})();

