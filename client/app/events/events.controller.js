(function () {
    angular
        .module("StackUp")
        .controller("EventsPageAppCtrl", EventsPageAppCtrl);

    function EventsPageAppCtrl() {
        var vm = this;
        console.log("EventsPageAppCtrl");

        function initNavBar () {
            (function($) {
                "use strict";

                var $navbar = $("#navbar"),
                    y_pos = $navbar.offset().top,
                    height = $navbar.height();

                $(document).scroll(function() {
                    var scrollTop = $(this).scrollTop();

                    if (scrollTop > y_pos + height) {
                        $navbar.addClass("navbar-fixed").animate({
                            top: 0
                        });
                    } else if (scrollTop <= y_pos) {
                        $navbar.removeClass("navbar-fixed").clearQueue().animate({
                            top: "-48px"
                        }, 0);
                    }
                });

            })(jQuery, undefined);
        }

        initNavBar();

        setCarouselHeight('#carousel-example');

        function setCarouselHeight(id)
        {
            var slideHeight = [];
            $(id+' .item').each(function()
            {
                // add all slide heights to an array
                slideHeight.push($(this).height());
            });

            // find the tallest item
            max = Math.max.apply(null, slideHeight);

            // set the slide's height
            $(id+' .carousel-content').each(function()
            {
                $(this).css('height',max+'px');
            });
        }

    }
})();